#include <iostream>
#include <cstring>

using namespace std;

const int MAX_LEN = 10;

struct Person
{
	char name[MAX_LEN];
	unsigned int age;
};

struct ListElement
{
	Person person;
	ListElement *next;
	ListElement *prev;
};

ListElement* get_last(ListElement *first)
{
	if(NULL == first)
	{
		return first;
	}

	ListElement *curr = first;
	while(curr->next != NULL)
	{
		curr = curr->next;
	}
	return curr;
};

void add_person(ListElement* &first, Person &person)
{
	ListElement *last = get_last(first);
	ListElement *element = new ListElement();
	strcpy(element->person.name, person.name);
	element->person.age = person.age;
	element->next = NULL;
	element->prev = NULL;

	if(last != NULL)
	{
		last->next = element;
		element->prev = last;
	}
	else
	{
		last = element;
	}

	if(NULL == first)
	{
		first = last;
	}
};

void print_list(ListElement *first, bool reversed = false)
{
	ListElement *curr = first;

	if(reversed)
	{
		curr = get_last(first);
	}

	while(curr != NULL)
	{
		cout<<curr->person.name<<endl;
		cout<<curr->person.age<<endl;
		if(reversed)
		{
			curr = curr->prev;
		}
		else
		{
			curr = curr->next;
		}
	}
}

void free_list(ListElement *first)
{
	ListElement *curr = first;
	while(curr != NULL)
	{
		ListElement *next = curr->next;
		delete curr;
		curr = next;
	}
}

int main(int argc, char *argv[])
{
	char name[MAX_LEN];
	unsigned int age;
	unsigned int number = 1;

	ListElement *first = NULL;

	while(true)
	{
		Person person;
		cout<<"Person number: "<<number++<<endl;
		cout<<"Name: ";

		cin.getline(person.name, MAX_LEN);
		if(0 == strlen(person.name))
		{
			break;
		}
		cout<<"Age: ";
		cin>>person.age;

		cin.clear();
		cin.ignore(10, '\n');
		add_person(first, person);
	}

	print_list(first, true);
	free_list(first);

	return 0;
}
