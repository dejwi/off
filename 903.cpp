#include <iostream>

using namespace std;

void print_matrix(int **array, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<cols; j++)
		{
			cout<<"| "<<array[i][j]<<" ";
		}
		cout<<'|'<<endl;
	}
}

int** matrix_alloc(int rows, int cols)
{
	int val = 0;
	int **array = new int*[rows];
	for(int i=0; i<rows; i++)
	{
		array[i] = new int[cols];
		for(int j=0; j<cols; j++)
		{
			array[i][j] = val++ % 5;
		}
	}
	return array;
}

void matrix_free(int **array, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		delete[] array[i];
	}
	delete[] array;
}

void matrix_transpose(int** &array, int &rows, int &cols)
{
	int **transposed = matrix_alloc(cols, rows);

	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<cols; j++)
		{
			transposed[j][i] = array[i][j];
		}
	}

	matrix_free(array, rows, cols);

	array = transposed;
	int tmp = rows;
	rows = cols;
	cols = tmp;
}

int main(int argc, char *argv[])
{
	int rows = 3;
	int cols = 4;
	int val = 0;
	int **array = matrix_alloc(rows, cols);

	print_matrix(array, rows, cols);

	cout<<"After transpose:"<<endl;

	matrix_transpose(array, rows, cols);
	print_matrix(array, rows, cols);

	matrix_free(array, rows, cols);

	return 0;
}
