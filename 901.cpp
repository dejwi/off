#include <iostream>

using namespace std;

bool swapstr(const char* &str1, const char* &str2)
{
	if(str1 == str2)
	{
		return false;
	}

	const char *tmp = str1;
	str1 = str2;
	str2 = tmp;

	return true;
}

int main(int argc, char *argv[])
{
	const char *first = "First string";
	const char *second = "Second string";

	cout<<"First string: "<<first<<endl;
	cout<<"Second string: "<<second<<endl;

	swapstr(first, second);

	cout<<"After swap"<<endl;

	cout<<"First string: "<<first<<endl;
	cout<<"Second string: "<<second<<endl;

	return 0;
}
