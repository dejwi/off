#include <iostream>

using namespace std;

void swap(char *txt, int first, int second)
{
	char tmp = txt[first];
	txt[first] = txt[second];
	txt[second] = tmp;
}

int main(int argc, char *argv[])
{
	char txt[] = "Hello World!";

	cout<<txt<<endl;
	cout<<"After swaping of 0th and 6th element"<<endl;
	swap(txt, 0, 6);
	cout<<txt<<endl;
	return 0;
}
