#include <iostream>

using namespace std;

const int ROWS = 4;
const int COLS = 6;


void print_matrix(int array[ROWS][COLS])
{
	for(int i=0; i<ROWS; i++)
	{
		for(int j=0; j<COLS; j++)
		{
			cout<<"| "<<array[i][j]<<" ";
		}
		cout<<'|'<<endl;
	}
}


void swap(int array[ROWS][COLS])
{
	for(int i=0; i<COLS/2; i++)
	{
		for(int j=0; j<ROWS; j++)
		{
			int tmp = array[j][i];
			array[j][i] = array[j][COLS-i-1];
			array[j][COLS-i-1] = tmp;
		}
	}
}

int main(int argc, char *argv[])
{
	int array[ROWS][COLS];

	int val = 0;
	for(int i=0; i<ROWS; i++)
	{
		for(int j=0; j<COLS; j++)
		{
			array[i][j] = val++ % 5;
		}
	}

	print_matrix(array);

	cout<<"After swapping of columns"<<endl;
	swap(array);

	print_matrix(array);
	return 0;
}
