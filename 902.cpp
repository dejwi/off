#include <iostream>

using namespace std;

void print_matrix(int **array, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<cols; j++)
		{
			cout<<"| "<<array[i][j]<<" ";
		}
		cout<<'|'<<endl;
	}
}

void matrix_mul(int **array, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<cols; j++)
		{
			array[i][j] = array[i][j] * 2;
		}
	}
}

int main(int argc, char *argv[])
{
	const int ROW_DIM = 3;
	const int COL_DIM = 4;
	int val = 0;
	int **array = new int*[ROW_DIM];
	for(int i=0; i<ROW_DIM; i++)
	{
		array[i] = new int[COL_DIM];
		for(int j=0; j<COL_DIM; j++)
		{
			array[i][j] = val++ % 5;
		}
	}

	print_matrix(array, ROW_DIM, COL_DIM);

	cout<<"After multiplication:"<<endl;

	matrix_mul(array, ROW_DIM, COL_DIM);
	print_matrix(array, ROW_DIM, COL_DIM);

	for(int i=0; i<ROW_DIM; i++)
	{
		delete[] array[i];
	}
	delete[] array;
	return 0;
}
