#include <iostream>

using namespace std;

void print_matrix(int **array, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<cols; j++)
		{
			cout<<"| "<<array[i][j]<<" ";
		}
		cout<<'|'<<endl;
	}
}

int** matrix_alloc(int rows, int cols)
{
	int val = 0;
	int **array = new int*[rows];
	for(int i=0; i<rows; i++)
	{
		array[i] = new int[cols];
		for(int j=0; j<cols; j++)
		{
			array[i][j] = val++ % 5;
		}
	}
	return array;
}

void matrix_free(int **array, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		delete[] array[i];
	}
	delete[] array;
}

bool swaparr(int* &arr1, int* &arr2)
{
	if(arr1 == arr2)
	{
		return false;
	}

	int *tmp = arr1;
	arr1 = arr2;
	arr2 = tmp;

	return true;
}

//zamienia wiersze, a nie kolumny. Nie doczytalem na starcie.
//teraz mi sie nie chcialo.
//
//Poprawka tego to zwyczajna zamiana relacji wiersz-kolumna.

void swap_rows(int** &array, int &rows, int &cols)
{
	for(int i=0; i<rows/2; i++)
	{
		swaparr(array[i], array[rows-i-1]);
	}
}

int main(int argc, char *argv[])
{
	int rows = 5;
	int cols = 4;
	int val = 0;
	int **array = matrix_alloc(rows, cols);

	print_matrix(array, rows, cols);

	cout<<"After swap:"<<endl;

	swap_rows(array, rows, cols);
	print_matrix(array, rows, cols);

	matrix_free(array, rows, cols);

	return 0;
}
