CPP_FILES := $(wildcard *.cpp)
EXE_FILES := $(addprefix exe/, $(CPP_FILES:.cpp=))

all: $(EXE_FILES)
	@echo All cpp files: $(CPP_FILES)
	@echo All executables: $(EXE_FILES)

exe/%: %.cpp
	@echo compiling [$< into $@]
	g++ -o $@ $<

clean:
	@rm exe/*
